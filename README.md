# Adot Technical Assessment

## Technical requirements

This project was developed in order to meet the following requirements :
- Each component is a functionnal React component
- Everything is written in Typescript (`.tsx` extension)
- Tests are written with Jest and @testing-library/react
- Each stylesheet is using SASS
- React context and localStorage are both used to store data
- The code is in English but all the wording is in French for consistency

## Specifications

This application is composed of a destinations list, a button labeled as "+ Ajouter" that allows the user to add a destination and a modal containing the addition form. 

On launch, this React app browses the user's localStorage looking for a destinations list. If none is found, a static one is displayed (and added to the localStorage) so the "empty" placeholder should never be shown.

Each destination has the following fields so the user needs to fill all of them in the form to enable the "Confirmer" button and add their destination :
- a name
- an address (focused on modal's opening)
- a link to an online picture
- a number of inhabitants
- an area (km²)
- a number of hotels
- a mean revenue (€)
- an active status (true or false)

As soon as the form is correctly filled and the button clicked, the user can see their destination being added to the list (visually and in the context/localStorage) and the modal will close. Anyways, if they change their mind, the creation modal can be closed by :
- clicking on the "Annuler" button
- clicking outside the modal
- pressing the ESC key
All these solutions will empty the form.

NB : the localStorage doesn't seem to be available on Microsoft Edge.

## Available Scripts

This application has been initialized using the `create-react-app` command with NPM as package manager. The following commands can therefore be useful.

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.
There 8 tests gathered in 3 tests suites :
- general interaction tests (`App.test.tsx`)
- numeric data formatting tests (`Destination.test.tsx`)
- creation form interaction and item addition (`DestinationCreation.text.tsx`)