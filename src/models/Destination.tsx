export default class Destination {
    private static index: number = 0;
    private imageLink: string;
    private name: string;
    private address: string;
    private active: boolean;
    private inhabitants: number;
    private hotels: number;
    private meanRevenue: number;
    private area: number;
    private key: number;

    constructor(imageLink: string, name: string, address: string, active: boolean, inhabitants: number, hotels: number, meanRevenue: number, area: number) {
        this.key = Destination.index++;
        this.imageLink = imageLink;
        this.name = name;
        this.address = address;
        this.active = active;
        this.inhabitants = inhabitants;
        this.hotels = hotels;
        this.meanRevenue = meanRevenue;
        this.area = area;
    }

    getKey(): number { return this.key }

    getImageLink(): string { return this.imageLink; }

    getName() { return this.name; }

    getAddress() { return this.address; }

    getActive() { return this.active; }

    getInhabitants() { return this.inhabitants; }

    getHotels() { return this.hotels; }

    getMeanRevenue() { return this.meanRevenue; }

    getArea() { return this.area; }

    setActive(newValue: boolean) { this.active = newValue; }
}

export interface DestinationListContext {
    destinations: Destination[]
}

const destination1 = new Destination("https://wallpapercave.com/wp/wp2055789.jpg", "Copenhagen", "Rådhuspladsen 1, 1550 København, Denmark", false, 3400000, 5000, 70000, 88.25);
const destination2 = new Destination("https://i.pinimg.com/originals/69/45/d8/6945d82fa5215814e40ec4e0263d3fa9.jpg", "Tehran", "Tehran, Azadi Square, Iran", false, 8600000, 400, 30000, 730);
const destination3 = new Destination("https://hdconsulting-paris.fr/wp-content/uploads/2017/07/Paris.jpg", "Paris", "Pl. de l'Hôtel de Ville, 75004 Paris", true, 2100000, 7500, 50000, 105.4);

export const destinationListRaw: Destination[] = [destination1, destination2, destination3];

export function formatNumber(value: number) {
    if(value < 1000) {
        const hundreds = value.toFixed(2)
        return removeUselessZeros(parseFloat(hundreds).toLocaleString('fr')).replace(',', '.')
    }
    if(value < 1000000) {
        const thousands = Math.round(value)
        return thousands.toLocaleString('fr')
    }
    const millions = Math.round(value/10000)/100
    return removeUselessZeros(millions.toLocaleString('fr')).replace(',', '.')+" M"
}

export function removeUselessZeros(value: string) {
    if(!value.includes(".")) return value
    const indexOfZero = value.indexOf('0')
    return value.substring(0, indexOfZero)
}
