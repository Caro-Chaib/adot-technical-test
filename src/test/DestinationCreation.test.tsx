/* eslint-disable testing-library/no-node-access */
import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import App from '../App';

test('disables "Confirmer" when form is empty', () => {
  render(<App />);
  expect(screen.getByRole('button')).toHaveTextContent('+ AJOUTER')
  fireEvent.click(screen.getByText('+ AJOUTER'))
  expect(screen.getByText('Ajouter une nouvelle destination')).toBeInTheDocument()
  // eslint-disable-next-line testing-library/no-node-access
  expect(screen.getByText('CONFIRMER').closest('button')).toHaveAttribute('disabled')
});

test('disables "Confirmer" until form is complete', () => {
    render(<App />)
    expect(screen.getByRole('button')).toHaveTextContent('+ AJOUTER')
    fireEvent.click(screen.getByText('+ AJOUTER'))
    expect(screen.getByText('Ajouter une nouvelle destination')).toBeInTheDocument()
    expect(screen.getByText('CONFIRMER').closest('button')).toHaveAttribute('disabled')

    const inputName = screen.getAllByPlaceholderText('Nom de la destination')[0].closest('input')
    if(inputName) fireEvent.change(inputName, {target: {value: 'New York'}})
    expect(screen.getByText('CONFIRMER').closest('button')).toHaveAttribute('disabled')

    const inputAddress = screen.getAllByPlaceholderText('Adresse')[0].closest('input')
    if(inputAddress) fireEvent.change(inputAddress, {target: {value: 'New York, NY 10004, United States'}})
    expect(screen.getByText('CONFIRMER').closest('button')).toHaveAttribute('disabled')

    const inputImage = screen.getAllByPlaceholderText('Lien de l\'image')[0].closest('input')
    if(inputImage) fireEvent.change(inputImage, {target: {value: 'https://img.static-af.com/images/media/462685A9-F284-44F5-83EC296F96B0C96D/'}})
    expect(screen.getByText('CONFIRMER').closest('button')).toHaveAttribute('disabled')

    const inputHabitants = screen.getAllByPlaceholderText('Nb. Habitants')[0].closest('input')
    if(inputHabitants) fireEvent.change(inputHabitants, {target: {value: 8400000}})
    expect(screen.getByText('CONFIRMER').closest('button')).toHaveAttribute('disabled')

    const inputHotels = screen.getAllByPlaceholderText('Nb. Hôtels')[0].closest('input')
    if(inputHotels) fireEvent.change(inputHotels, {target: {value: 12000}})
    expect(screen.getByText('CONFIRMER').closest('button')).toHaveAttribute('disabled')

    const inputRevenue = screen.getAllByPlaceholderText('Revenu Moy')[0].closest('input')
    if(inputRevenue) fireEvent.change(inputRevenue, {target: {value: 100000}})
    expect(screen.getByText('CONFIRMER').closest('button')).toHaveAttribute('disabled')

    const inputArea = screen.getAllByPlaceholderText('Superficie')[0].closest('input')
    if(inputArea) fireEvent.change(inputArea, {target: {value: 738.8}})
    expect(screen.getByText('CONFIRMER').closest('button')).not.toHaveAttribute('disabled')
  });

  test('closes modal and adds new destination when "Confirmer" clicked', () => {
    render(<App />)
    expect(screen.getByRole('button')).toHaveTextContent('+ AJOUTER')
    fireEvent.click(screen.getByText('+ AJOUTER'))
    expect(screen.getByText('Ajouter une nouvelle destination')).toBeInTheDocument()

    const inputName = screen.getAllByPlaceholderText('Nom de la destination')[0].closest('input')
    if(inputName) fireEvent.change(inputName, {target: {value: 'New York'}})

    const inputAddress = screen.getAllByPlaceholderText('Adresse')[0].closest('input')
    if(inputAddress) fireEvent.change(inputAddress, {target: {value: 'New York, NY 10004, United States'}})

    const inputImage = screen.getAllByPlaceholderText('Lien de l\'image')[0].closest('input')
    if(inputImage) fireEvent.change(inputImage, {target: {value: 'https://img.static-af.com/images/media/462685A9-F284-44F5-83EC296F96B0C96D/'}})

    const inputHabitants = screen.getAllByPlaceholderText('Nb. Habitants')[0].closest('input')
    if(inputHabitants) fireEvent.change(inputHabitants, {target: {value: 8400000}})

    const inputHotels = screen.getAllByPlaceholderText('Nb. Hôtels')[0].closest('input')
    if(inputHotels) fireEvent.change(inputHotels, {target: {value: 12000}})

    const inputRevenue = screen.getAllByPlaceholderText('Revenu Moy')[0].closest('input')
    if(inputRevenue) fireEvent.change(inputRevenue, {target: {value: 100000}})

    const inputArea = screen.getAllByPlaceholderText('Superficie')[0].closest('input')
    if(inputArea) fireEvent.change(inputArea, {target: {value: 738.8}})
    expect(screen.getByText('CONFIRMER').closest('button')).not.toHaveAttribute('disabled')

    fireEvent.click(screen.getByText('CONFIRMER'))
    expect(screen.queryByText('Ajouter une nouvelle destination')).not.toBeInTheDocument()
    expect(screen.getByText('New York')).toBeInTheDocument()
  });