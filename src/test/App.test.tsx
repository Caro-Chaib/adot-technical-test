import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import App from '../App';
import userEvent from '@testing-library/user-event';

test('opens creation modal with "Ajouter" button', () => {
  render(<App />);
  expect(screen.getByRole('button')).toHaveTextContent('+ AJOUTER')
  fireEvent.click(screen.getByText('+ AJOUTER'))
  expect(screen.getByText('Ajouter une nouvelle destination')).toBeInTheDocument()
});

test('closes creation modal with "Annuler" button', () => {
  render(<App />);
  fireEvent.click(screen.getByText('+ AJOUTER'))
  expect(screen.getByText('Ajouter une nouvelle destination')).toBeInTheDocument()
  fireEvent.click(screen.getByText('ANNULER'))
  expect(screen.queryByText('Ajouter une nouvelle destination')).not.toBeInTheDocument()
});

test('closes creation modal with ESC key', () => {
  render(<App />);
  fireEvent.click(screen.getByText('+ AJOUTER'))
  expect(screen.getByText('Ajouter une nouvelle destination')).toBeInTheDocument()
  fireEvent.keyDown(screen.getByText(/Ajouter une nouvelle destination/i), {
    key: "Escape",
    code: "Escape",
    keyCode: 27,
    charCode: 27
  });
  expect(screen.queryByText('Ajouter une nouvelle destination')).not.toBeInTheDocument()
});

test('closes creation modal by clicking outside', () => {
  render(<App />);
  fireEvent.click(screen.getByText('+ AJOUTER'))
  expect(screen.getByText('Ajouter une nouvelle destination')).toBeInTheDocument()
  userEvent.click(screen.getByRole('dialog'))
  expect(screen.queryByText('Ajouter une nouvelle destination')).not.toBeInTheDocument()
});
