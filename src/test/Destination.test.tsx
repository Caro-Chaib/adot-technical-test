import assert from 'assert';
import { formatNumber } from '../models/Destination';

test('formats numeric values by magnitude', () => {
    assert(formatNumber(45.60) === '45.6')
    assert(formatNumber(45.634) === '45.63')
    assert(formatNumber(5345.70) === '5 346')
    assert(formatNumber(5345.20) === '5 345')
    assert(formatNumber(1750345.30) === '1.75 M')
    assert(formatNumber(100881342) === '100.88 M')
    assert(formatNumber(100885342) === '100.89 M')
});