import React, { useState } from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import { Modal } from 'react-bootstrap';
import Form from "react-bootstrap/Form";
import Switch from 'react-switch'
import Destination, { DestinationListContext } from '../../models/Destination';
import Button from '../shared/Button'
import Input from '../shared/Input';
import './CreationModal.scss';

interface CreationModalProps {
  destinations: DestinationListContext | null,
  setShow: React.Dispatch<React.SetStateAction<boolean>>,
  show: boolean | undefined,
  destinationsStateAltering: React.Dispatch<React.SetStateAction<Destination[]>>,
  destinationsState: Destination[],
  children: never[]
}

export default function CreationModal(props: CreationModalProps) {

  function validateCreationForm(name: string, address: string, link: string, inhabitants: number, hotels: number, revenue: number, area: number) {
    const checkNumericValues = inhabitants > 0 && hotels > 0 && revenue > 0 && area > 0;
    const checkStringValues = name.length > 0 && address.length > 0 && link.length > 0;
    return checkNumericValues && checkStringValues;
  }
  
  const handleClose = () => props.setShow(false);

  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [link, setLink] = useState("");
  const [inhabitants, setInhabitants] = useState(0);
  const [hotels, setHotels] = useState(0);
  const [revenue, setRevenue] = useState(0);
  const [area, setArea] = useState(0);
  const [active, setActive] = useState(true);

  function addDestination() {
    if(props.destinations !== null) {
      props.destinations.destinations.push(new Destination(link, name, address, active, inhabitants, hotels, revenue, area));
      props.destinationsStateAltering(props.destinations.destinations)
    }
    handleClose()
  }
  
  return (
    <>
      <div className="custom-creation-modal">
        <Modal show={props.show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">
              Ajouter une nouvelle destination
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Container>
              <Form id="modalForm">
                <Row>
                  <Col xs={12} className="modal-fields">
                    <Form.Group>
                      <Input type="text" placeholder="Nom de la destination" onChange={(e: React.ChangeEvent<HTMLInputElement>) => setName(e.target.value)} min={0} disabled={false} autofocus={false}></Input>
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12} className="modal-fields">
                    <Form.Group>
                      <Input type="text" autofocus={true} placeholder="Adresse" onChange={(e: React.ChangeEvent<HTMLInputElement>) => setAddress(e.target.value)} min={0} disabled={false}></Input>
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12} className="modal-fields">
                    <Form.Group>
                      <Input type="text" placeholder="Lien de l'image" onChange={(e: React.ChangeEvent<HTMLInputElement>) => setLink(e.target.value)} min={0} disabled={false} autofocus={false}></Input>
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col md={3} sm={6} xs={12} className="modal-fields">
                    <Form.Group>
                      <Input type="number" min={0} placeholder="Nb. Habitants" onChange={(e: React.ChangeEvent<HTMLInputElement>) => setInhabitants(parseInt(e.target.value))} disabled={false} autofocus={false}></Input>
                    </Form.Group>
                  </Col>
                  <Col md={3} sm={6} xs={12} className="modal-fields">
                    <Form.Group>
                      <Input type="number" min={0} placeholder="Nb. Hôtels" onChange={(e: React.ChangeEvent<HTMLInputElement>) => setHotels(parseInt(e.target.value))} disabled={false} autofocus={false}></Input>
                    </Form.Group>
                  </Col>
                  <Col md={3} sm={6} xs={12} className="modal-fields">
                    <Form.Group>
                      <Input type="number" min={0} placeholder="Revenu Moy" onChange={(e: React.ChangeEvent<HTMLInputElement>) => setRevenue(parseInt(e.target.value))} disabled={false} autofocus={false}></Input>
                    </Form.Group>
                  </Col>
                  <Col md={3} sm={6} xs={12} className="modal-fields">
                    <Form.Group>
                      <Input type="number" min={0} placeholder="Superficie" onChange={(e: React.ChangeEvent<HTMLInputElement>) => setArea(parseInt(e.target.value))} disabled={false} autofocus={false}></Input>
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <label className="switch-container">
                    <Switch 
                        boxShadow="0px 1px 3px 0px #727272"
                        onChange={setActive} 
                        checked={active} 
                        checkedIcon={false} 
                        uncheckedIcon={false}
                        onColor="#aceccd"
                        offColor="#c5c5c5"
                        onHandleColor="#3fd48b"
                        offHandleColor="#e6e6e6"
                        handleDiameter={21}
                        height={14}
                        width={38}>
                    </Switch>
                    <span className="switch-label">Activer</span>
                  </label>
                </Row>
              </Form>
            </Container>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={handleClose} content="Annuler" backgroundColor="transparent" color="#c5c5c5" uppercaseLabel={true}></Button>
            <Button onClick={addDestination} disabled={!validateCreationForm(name, address, link, inhabitants, hotels, revenue, area)} form="modalForm" type="submit" content="Confirmer" backgroundColor="transparent" color="#3fd48b" uppercaseLabel={true}></Button>
          </Modal.Footer>
        </Modal>
      </div>
    </>
  );
}