import React, { useEffect, useRef } from 'react';
import Form from "react-bootstrap/Form";
import './Input.scss';

interface InputProps {
    type: string | undefined,
    onChange: React.ChangeEventHandler | undefined,
    placeholder: string | undefined,
    min: number,
    disabled: boolean,
    autofocus: boolean
}

function Input(props: InputProps) {
    const focusInputRef = useRef<HTMLInputElement>(null);

    useEffect(()=>{
        focusInputRef.current?.focus();
    }, []);

    return (
        <Form.Control 
            className="custom-input"
            type={props.type}
            placeholder={props.placeholder}
            onChange={props.onChange}
            min={props.min}
            disabled={props.disabled}
            autoFocus={props.autofocus}
            ref={props.autofocus ? focusInputRef : null}
        />
    );
}

export default Input;