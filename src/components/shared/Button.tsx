import React from 'react';
import { ButtonProps } from 'react-bootstrap';
import './Button.scss';

interface CustomButtonProps extends ButtonProps {
    backgroundColor: string | undefined,
    content: string | undefined,
    uppercaseLabel: boolean
}

function Button(props: CustomButtonProps) {
    return (
        <button 
            className="custom-button"
            type={props.type}
            disabled={props.disabled}
            onClick={props.onClick}
            style={{ 
                backgroundColor: props.backgroundColor, 
                color: props.color }
            }
        >{props.uppercaseLabel ? props.content?.toUpperCase() : props.content}</button>
    );
}

export default Button;