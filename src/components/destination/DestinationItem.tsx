import React, { useState } from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import './DestinationItem.scss';
import Switch from 'react-switch'
import Destination, { formatNumber } from '../../models/Destination';

interface DestinationItemProps {
    item: Destination
}

export default function DestinationItem(props: DestinationItemProps) {

    const [activeState, setActiveState] = useState({ checked: props.item.getActive() })

    function handleChange(checked: boolean) {
        setActiveState({ checked });
        props.item.setActive(checked);
    }
    
    handleChange.bind(activeState)

    return (
        <Container className="Card">
        <header>
            <img src={props.item.getImageLink()} alt={props.item.getName()}></img>
        </header>
        <div className="body">
            <div className="header">
                <div className="title">
                    <h2>{props.item.getName()}</h2>
                    <h4>{props.item.getAddress()}</h4>
                </div>
                <Switch 
                    boxShadow="0px 1px 3px 0px #727272"
                    onChange={handleChange} 
                    checked={activeState.checked} 
                    checkedIcon={false} 
                    uncheckedIcon={false}
                    onColor="#aceccd"
                    offColor="#c5c5c5"
                    onHandleColor="#3fd48b"
                    offHandleColor="#e6e6e6"
                    handleDiameter={21}
                    height={14}
                    width={38}>
                </Switch>
            </div>
            <hr style={{
                color: '#c5c5c5',
                backgroundColor: '#c5c5c5',
                height: .5,
                borderColor : '#c5c5c5'
            }}/>
            <Row className="detail">
                <Col xs={3}>
                    <p className='value'>{formatNumber(props.item.getInhabitants() ?? 0)}</p>
                    <p className='unit'>Habitants</p>
                </Col>
                <Col xs={3}>
                    <p className='value'>{formatNumber(props.item.getHotels() ?? 0)}</p>
                    <p className='unit'>Hôtels</p>
                </Col>
                <Col xs={3}>
                    <p className='value'>{formatNumber(props.item.getMeanRevenue() ?? 0)}€</p>
                    <p className='unit'>Revenu Moy</p>
                </Col>
                <Col xs={3}>
                    <p className='value'>{formatNumber(props.item.getArea() ?? 0)}</p>
                    <p className='unit'>km²</p>
                </Col>
            </Row>
        </div>
        </Container>
    );
}