import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import Destination, { DestinationListContext } from '../../models/Destination';
import DestinationItem from './DestinationItem';
import './DestinationList.scss';

function createDestination(destination: Destination) {
    return (<Col lg={4} md={6} xs={12} key={destination.getKey()}><DestinationItem item={destination}></DestinationItem></Col>)
}

function DestinationList(props: DestinationListContext) {
    if(props.destinations.length > 0) {
        return (
            <Container>
                <Row>
                    {props.destinations.map(createDestination)}
                </Row>
            </Container>
        );
    }
    return (
        <Container>
            <Row>
                <h4>Pas de destinations disponibles... Ajoutez-en grâce au bouton !</h4>
            </Row>
        </Container>
    );
}

export default DestinationList;