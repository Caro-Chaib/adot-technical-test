import React, { useState, useCallback, useEffect } from 'react';
import './App.scss';
import DestinationList from './components/destination/DestinationList';
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from './components/shared/Button';
import CreationModal from './components/modals/CreationModal';
import Destination, { DestinationListContext, destinationListRaw } from './models/Destination';
import useStateWithCallback from 'use-state-with-callback';

function isEmpty(value: string | null){
  return (value == null || value.length === 0);
}

export default function App() {
  const [showModal, setShowModal] = useState(false);
  const handleShowModal = () => { setShowModal(true) }

  const escFunction = useCallback((event: KeyboardEvent) => {
    if (event.key === '27') {
      setShowModal(false);
    }
  }, []);

  useEffect(() => {
    document.addEventListener("keydown", escFunction);
    return () => {
      document.removeEventListener("keydown", escFunction);
    };
  }, [escFunction]);

  const destinationsDefault = (!isEmpty(localStorage.getItem('destinations'))) ? JSON.parse(localStorage.getItem('destinations') ?? "").map(
    (e: { imageLink: string; name: string; address: string; active: boolean; inhabitants: number; hotels: number; meanRevenue: number; area: number; }) => {
      return new Destination(e.imageLink, e.name, e.address, e.active, e.inhabitants, e.hotels, e.meanRevenue, e.area)
    })
  : destinationListRaw

  const [destinationsState, setDestinations] = useStateWithCallback<Destination[]>(
    destinationsDefault, destinations => {
      setDestinations(destinations)
      localStorage.setItem('destinations', JSON.stringify(destinations))
    }
  )

  const DestinationsContext = React.createContext<DestinationListContext | null>(null);

  const destinationListContext: DestinationListContext = {destinations: destinationsState};

  useEffect(() => {
    const destinationsStorage = localStorage.getItem('destinations')
    if(destinationsStorage !== null) {
      const destinationsStorageParsed = JSON.parse(destinationsStorage)
      setDestinations(destinationsStorageParsed.map((e: { imageLink: string; name: string; address: string; active: boolean; inhabitants: number; hotels: number; meanRevenue: number; area: number; }) => {
        return new Destination(e.imageLink, e.name, e.address, e.active, e.inhabitants, e.hotels, e.meanRevenue, e.area)
      }))
    }
  })

  function PostDestinationsFromContext() {
    const contextDestinations = React.useContext(DestinationsContext);
    return <DestinationList destinations={contextDestinations?.destinations ?? []}></DestinationList>;
  }

  function CreationModalFromContext() {
    const contextDestinations = React.useContext(DestinationsContext);
    return <CreationModal
      show={showModal}
      setShow={setShowModal}
      destinationsState={destinationsState}
      destinationsStateAltering={setDestinations}
      destinations={contextDestinations}>  
    </CreationModal>
  }

  return (
    <div className="App">
      <div className="App-container">
        <header className="App-header">
          <h1>
            Destinations
          </h1>
          <Button 
            backgroundColor="#3fd48b"
            color="white"
            content="+ Ajouter"
            uppercaseLabel={true}
            onClick={handleShowModal}></Button>
        </header>
        <DestinationsContext.Provider value={destinationListContext}>
          <PostDestinationsFromContext/>
          <CreationModalFromContext/>
        </DestinationsContext.Provider>
      </div>
    </div>
  );
}
